package com.miador.hibernate.demo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.miador.hibernate.demo.entity.Student;

public class QueryStudentDemo extends JFrame implements ActionListener {

	private JTable table;
	private DefaultTableModel tableModel;
	private JPopupMenu popupMenu;
	private JMenuItem menuItemAdd;
	private JMenuItem menuItemRemove;
	private JMenuItem menuItemRemoveAll;
	List<Student> theStudents;
	SessionFactory factory;
	Session session;

	public QueryStudentDemo() throws Exception{

		// create session factory

		 factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();

		// create session

		 session = factory.getCurrentSession();
		
		
		try {

			// start a transaction

			session.beginTransaction();
			
			// query students
			theStudents = session.createQuery("from Student").list();
			String[] cols = { "id", "first_name", "last_name", "email" };
			
			String[][] rowDataTable = new String[theStudents.size()][4];
			
			
			for(int i = 0 ; i < theStudents.size();i++) {
				rowDataTable[i][0] = String.valueOf(theStudents.get(i).getId());
				rowDataTable[i][1] = theStudents.get(i).getFirstName();
				rowDataTable[i][2] = theStudents.get(i).getLastName();
				rowDataTable[i][3] = theStudents.get(i).getEmail();
	
			}
			
			tableModel = new DefaultTableModel(rowDataTable,cols);
			table = new JTable(tableModel);
						
			popupMenu = new JPopupMenu();
			menuItemAdd = new JMenuItem("Add New Row");
			menuItemRemove = new JMenuItem("Remove Current Row");
			menuItemRemoveAll = new JMenuItem("Remove All Rows");
			
			menuItemAdd.addActionListener(this);
			menuItemRemove.addActionListener(this);
			menuItemRemoveAll.addActionListener(this);
			
			popupMenu.add(menuItemAdd);
			popupMenu.add(menuItemRemove);
			popupMenu.add(menuItemRemoveAll);
			
			// sets the popup menu for the table
			table.setComponentPopupMenu(popupMenu);
			
			table.addMouseListener(new TableMouseListener(table));
			
			
			add(new JScrollPane(table));

			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setSize(800, 1000);
			setLocationRelativeTo(null);


			session.getTransaction().commit();
			System.out.println("Done!");

		} finally {
			//factory.close();
		}

	}

	public static void main(String[] args) throws Exception {
		
		new QueryStudentDemo().setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		JMenuItem menu = (JMenuItem) event.getSource();
		if (menu == menuItemAdd) {
			addNewRow();
		} else if (menu == menuItemRemove) {
			removeCurrentRow();
		} else if (menu == menuItemRemoveAll) {
			removeAllRows();
		}
		
	}
	private void removeAllRows() {
		int rowCount = tableModel.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			tableModel.removeRow(0);
		}
		
	}

	public void removeCurrentRow() {
		try {
		session = factory.getCurrentSession();
		session.beginTransaction();
		int selectedRow = table.getSelectedRow();
		tableModel.removeRow(selectedRow);
		
		Student myStudent = session.get(Student.class, theStudents.get(selectedRow).getId());
		session.delete(myStudent);
		session.getTransaction().commit();
		
			
		} finally {
			// TODO: handle exception
			factory.close();
		}
		
		
		
	}

	private void addNewRow() {
		tableModel.addRow(new String[0]);
		
	}



}
