package isaabimvc;

public interface BaseDao {
	abstract void save();
	abstract void updater();
	abstract void delete();
	abstract void saveWithoutHibernate();
}
